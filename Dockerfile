FROM python:3.6-slim

SHELL ["/bin/bash", "-c"]

RUN apt-get update -qq && \
  apt-get install -y --no-install-recommends \
  build-essential \
  -my wget gnupg \
  openssh-client \
  graphviz-dev \
  pkg-config \
  git-core \
  openssl \
  libssl-dev \
  libffi6 \
  libffi-dev \
  libpng-dev \
  curl && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
  mkdir /app \
  && curl --silent --location https://deb.nodesource.com/setup_10.x | bash - \
  && apt-get install -y nodejs

WORKDIR /app
ADD requirements.txt .
ADD requirements ./requirements

RUN pip install -r requirements.txt --no-cache-dir

ADD . /app
RUN npm install && npm run-script build

EXPOSE 5000 5000

ENV FLASK_ENV=production
ENV FLASK_APP=autoapp.py
CMD npm start