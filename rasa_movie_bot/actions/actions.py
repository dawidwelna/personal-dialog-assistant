import os
import json
import re
import pandas as pd
import requests
from dotenv import load_dotenv
from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet

global basedir
basedir = os.path.abspath(os.path.dirname(__file__))


def get_genre_id(genre):
    """
    Get the genre id by its name
    :param genre: str, genre name e.g. "comedy"
    :return: int, the id of given genre
    """
    with open(basedir + '/genres.json', 'r', encoding='utf-8') as f:
        genres = json.load(f)
        genres = genres['genres']
        genres = pd.DataFrame(genres)

    genre_id = int(genres.loc[genres['name'] == genre.lower().capitalize()].get('id'))
    return genre_id


def get_actor_id(actor, api_key):
    actor = actor.split(' ')
    name = actor[0]
    surname = actor[1]
    query = requests.get('https://api.themoviedb.org/3/search/person?api_key={0}&'
                         'language=en-US&query={1}%20{2}&page=1&include_adult=false'.format(
                          api_key, name, surname)).text

    try:
        actor = json.loads(query)
        actor = actor['results'][0]
        return actor.get('id')
    except:
        return None


def filter_adult_content(adult_content):
    if re.search('yes|true|yep|y|indeed|of course', adult_content):
        return False
    else:
        return True


def get_original_lang_code(original_language):
    original_language = original_language.lower().capitalize()
    data = pd.read_csv(basedir + '/original_languages.csv', encoding='utf-8', index_col=1)
    try:
        lang_code = data.loc[original_language].alpha2
    except KeyError:
        lang_code = None
    return lang_code


class ActionCheckMovie(Action):
    def name(self):
        return "action_check_movie"

    def run(self, dispatcher, tracker, domain):
        dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        api_key = os.getenv('TMDB_API_KEY')

        genre = tracker.get_slot('genre')
        if genre:
            genre_id = get_genre_id(genre)
        else:
            genre_id = None

        release_year = tracker.get_slot('release_year')

        adult_content = tracker.get_slot('adult_content')
        if adult_content:
            include_adult_content = filter_adult_content(adult_content)
        else:
            include_adult_content = None

        original_language = tracker.get_slot('original_language')
        if original_language:
            original_language_code = get_original_lang_code(original_language)
        else:
            original_language_code = None

        movies_details = None
        if release_year is not None and genre_id is not None and include_adult_content is not None:
            movies_details = requests.get('https://api.themoviedb.org/3/discover/'
                                          'movie?api_key={0}&'
                                          'language=en-US&sort_by=popularity.desc&include_adult={1}&'
                                          'include_video=false&page=1&'
                                          'primary_release_year={2}&'
                                          'with_genres={3}'.format(api_key,
                                                                   include_adult_content,
                                                                   release_year, genre_id)).text

        elif original_language_code is not None and genre_id is not None and include_adult_content is not None:
            movies_details = requests.get('https://api.themoviedb.org/3/discover/'
                                          'movie?api_key={0}&'
                                          'language=en-US&sort_by=popularity.desc&include_adult={1}&'
                                          'include_video=false&page=1&'
                                          'with_original_language={2}&'
                                          'with_genres={3}'.format(api_key,
                                                                   include_adult_content,
                                                                   original_language_code,
                                                                   genre_id)).text
        # if movies_details:
        details = json.loads(movies_details)
        results = details['results'][:3]

        responses = ''
        for res in results:
            poster = 'http://image.tmdb.org/t/p/w185/{}'.format(res['poster_path'])
            release_year = res['release_date'].split('-')[0]
            response = "{} ({})<br><img src={} width='185' height='278'><br>Plot: {}".format(res['title'], release_year,
                                                                                         poster, res['overview'])
            responses += response + '\n\n'

        dispatcher.utter_message(responses.rstrip())
        return [SlotSet('genre', None), SlotSet('release_year', None), SlotSet('actor', None),
                SlotSet('adult_content', None), SlotSet('original_language', None)]
        # else:
        #     dispatcher.utter_message("Sorry, I don't understand. Could you use other words?")


class ActionCheckMovieByActor(Action):
    def name(self):
        return "action_check_movie_by_actor"

    def run(self, dispatcher, tracker, domain):
        dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        api_key = os.getenv('TMDB_API_KEY')

        genre = tracker.get_slot('genre')
        if genre:
            genre_id = get_genre_id(genre)
        else:
            genre_id = None

        actor = tracker.get_slot('actor')
        if actor:
            actor_id = get_actor_id(actor, api_key)
        else:
            actor_id = None

        movies_details = None

        if actor_id is not None and genre_id is not None:
            movies_details = requests.get('https://api.themoviedb.org/3/discover/'
                                          'movie?api_key={0}&'
                                          'language=en-US&sort_by=popularity.desc&'
                                          'include_video=false&page=1&'
                                          'with_people={1}&'
                                          'with_genres={2}'.format(api_key,
                                                                   actor_id, genre_id)).text

        # if movies_details:
        details = json.loads(movies_details)
        results = details['results'][:3]

        responses = ''
        for res in results:
            poster = 'http://image.tmdb.org/t/p/w185/{}'.format(res['poster_path'])
            release_year = res['release_date'].split('-')[0]
            response = "{} ({})<br><img src={} width='185' height='278'><br>Plot: {}".format(res['title'], release_year,
                                                                                         poster, res['overview'])
            responses += response + '\n\n'

        dispatcher.utter_message(responses.rstrip())
        return [SlotSet('genre', None), SlotSet('release_year', None), SlotSet('actor', None),
                SlotSet('adult_content', None), SlotSet('original_language', None)]
        # else:
        #     dispatcher.utter_message("Sorry, I don't understand. Could you use other words?")

