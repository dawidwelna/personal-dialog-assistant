## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## regex:greet
- hey[^\s]*

## intent:specify_country
- [french](original_language) movie
- [english](original_language) movie
- [polish](original_language) movie
- I want to watch [english](original_language) movie
- I have desire to watch [italian](original_language) production
- language [polish](original_language)
- [Czech](original_language)
- [Chechen](original_language)
- [Welsh](original_language)
- [Spanish](original_language)
- movie in [French](original_language)
- movie in [German](original_language)
- movie in [Welsh](original_language)
- movie in [English](original_language)
- movie in [Italian](original_language)
- movie in [Czech](original_language)
- movie in [Spanish](original_language)
- movie in [french](original_language)
- movie in [german](original_language)
- movie in [welsh](original_language)
- movie in [english](original_language)
- movie in [italian](original_language)
- movie in [czech](original_language)
- movie in [spanish](original_language)
- [Italian](original_language) movie
- [German](original_language) movie
- [English](original_language) movie
- [Czech](original_language) movie
- [Chechen](original_language) movie
- [Welsh](original_language) movie
- I want [Spanish](original_language) movie
- I want [Italian](original_language) movie
- I want [German](original_language) movie
- I want [English](original_language) movie
- I want [Czech](original_language) movie
- I want [Chechen](original_language) movie
- I want [Welsh](original_language) movie
- I want movie in [Italian](original_language)
- I want movie in [German](original_language)
- I want movie in [English](original_language)
- I want movie in [Czech](original_language)
- I want movie in [Chechen](original_language)
- I want movie in [Welsh](original_language)
- I want movie in [Spanish](original_language) 
- a movie produced in [Italian](original_language) language
- a movie produced in [german](original_language) language
- a movie produced in [English](original_language) language
- a movie produced in [Czech](original_language) language
- a movie produced in [Chechen](original_language) language
- a movie produced in [Welsh](original_language) language
- a movie produced in [Spanish](original_language) language
- I have desire to watch [Polish](original_language) production
- I have desire to watch [italian](original_language) production
- I have desire to watch [English](original_language) production
- I have desire to watch [italian](original_language) production
- I have desire to watch [Chechen](original_language) production
- I have desire to watch [Welsh](original_language) production
- I have desire to watch [Spanish](original_language) production
- I want [spanish](original_language) movie
- I want [italian](original_language) movie
- I want [german](original_language) movie
- I want [english](original_language) movie
- I want [czech](original_language) movie
- I want [chechen](original_language) movie
- I want [welsh](original_language) movie
- I want movie in [italian](original_language)
- I want movie in [german](original_language)
- I want movie in [english](original_language)
- I want movie in [czech](original_language)
- I want movie in [chechen](original_language)
- I want movie in [welsh](original_language)
- I want movie in [spanish](original_language) 
- a movie produced in [italian](original_language) language
- a movie produced in [german](original_language) language
- a movie produced in [english](original_language) language
- a movie produced in [czech](original_language) language
- a movie produced in [chechen](original_language) language
- a movie produced in [welsh](original_language) language
- a movie produced in [spanish](original_language) language
- I have desire to watch [polish](original_language) production
- I have desire to watch [italian](original_language) production
- I have desire to watch [english](original_language) production
- I have desire to watch [italian](original_language) production
- I have desire to watch [chechen](original_language) production
- I have desire to watch [welsh](original_language) production
- I have desire to watch [spanish](original_language) production


## intent:movie_with_actor
- I want to watch a movie with [Jim Carrey](actor)
- I'd like to watch a movie with [Arnold Schwarzeneger](actor)
- The movie starring [John Travolta](actor)
- A movie with [Cezary Pazura](actor)
- the movie where [john travolta](actor) is acting
- the movie where [john travolta](actor) acts

## regex:movie_with_actor
- (with|starring|acting|acts)

## regex:actor
- [A-Za-z]{,10}\s[A-Za-z]{,10}

## intent:movie_want
- What you recommend? I want to watch a movie.
- What you recommend for me today, I want to watch a movie.
- What you could recommend for tonight, I want to watch a movie.
- What you could recommend?
- What you could recommend for today?
- Can you recommend a movie for me?
- I want to watch movie this evening.
- I would like to watch movie this evening.
- I'd like to watch movie tonight.
- I want to watch some movie
- I have desire for a movie today
- I have desire for a movie tonight
- I have desire for a movie this evening
- Is there a movie for me today?
- Is there a movie for me tonight?

## intent:specify_genre
- Tonight I have desire to watch [horror](genre) movie
- Give me something from [romantic comedy](genre)
- do you have some [adventure](genre) film?
- I have desire to see [action](genre)
- I'd like to watch [comedy](genre)
- I am the most interest in [Fantasy](genre)
- [comedy](genre)
- [family](genre) movie interests me
- [Science Fiction](genre)
- [sci-fi](genre:Science Fiction)
- genre [comedy](genre)
- genre [family](genre) interests me
- genre [Science Fiction](genre)
- genre [sci-fi](genre:Science Fiction)
- [comedy](genre) genre
- [family](genre) genre interests me
- [Science Fiction](genre)  genre
- [sci-fi](genre:Science Fiction) genre

## lookup:genre
- Action
- Adventure
- Animation
- Comedy
- Crime
- Documentary
- Drama
- Family
- Fantasy
- History
- Horror
- Music
- Mystery
- Romance
- Science Fiction
- TV Movie
- Thriller
- War
- Western
- action
- adventure
- animation
- comedy
- crime
- documentary
- drama
- family
- fantasy
- history
- horror
- music
- mystery
- romance
- science Fiction
- tv movie
- thriller
- war
- western


## intent:specify_year
- [1994](release_year)
- I want from [1936](release_year)
- release year [2004](release_year) 
- give me from [2018](release_year)
- movie from [2005](release_year)
- year [2010](release_year)

## regex:release_year
- [0-9]{4}

## intent:specify_filter
- [filter](adult_content) out
- do [filter](adult_content)
- do [filter](adult_content) adult content
- do [not filter](adult_content) adult content
- [yes](adult_content) of course
- [yes](adult_content) indeed
- [no](adult_content)
- [no](adult_content) never
- [of course](adult_content)
- [yep](adult_content)
- [indeed](adult_content)

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:mood_affirm
- that sounds good
- correct
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good

## intent:mood_deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremly sad
- so sad
