## movie by genre 1
* movie_want
  - utter_ask_genre
* specify_genre
  - utter_ask_year
* specify_year
  - utter_ask_country
* specify_country
  - utter_ask_filter
* specify_filter
  - action_check_movie
  - utter_did_that_help
> check_did_that_help


## movie by genre 2
* specify_genre
  - utter_ask_year
* specify_year
  - utter_ask_country
* specify_country
  - utter_ask_filter
* specify_filter
  - action_check_movie
  - utter_did_that_help
> check_did_that_help
  
  
## movie by actor 1
* movie_with_actor
  - utter_ask_genre
* specify_genre
  - action_check_movie_by_actor
  - utter_did_that_help
> check_did_that_help


## user denies
> check_did_that_help
* mood_deny
  - utter_goodbye
  
  
## user affirms
> check_did_that_help
* mood_affirm
  - utter_happy