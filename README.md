===============================
Spoken Dialog Assistant for Recommendation System
===============================

Recommendation system of movies to watch for evening
implementent in the form of Personal Digital Assistant(PDA) - chatbot.


setup
------
    .env file has to be located inside ./rasa_movie_bot/actions/
    with OMDB_API_KEY=your_api_key 


App Quickstart
----------

shortcut
------

    git clone (sdars_repository)
    
    cd (sdars_repository)
    
    sudo docker-compose up --build
    
    go to localhost:5000


manually
-----

First, set your app's secret key as an environment variable. For example,
add the following to ``.bashrc`` or ``.bash_profile``.

.. code-block:: bash

    export SDARS_SECRET='something-really-secret'    
    go to package.json and scripts/flask-server set to flask run --host=127.0.0.1
    also go to webpack.config.js and set publicHost tp 'http://127.0.0.1:2992'


Run the following commands to bootstrap your environment ::

    git clone ...
    cd imdb
    pip install -r requirements.txt

Very important before running "npm install"
Make sure to have nodejs >= 6.1.0

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt-get install -y nodejs

Then you can install node_modules

    npm install

build and start

    npm run-script build
    npm start  # run the webpack dev server and flask server using concurrently

You will see a pretty welcome screen.

In general, before running shell commands, set the ``FLASK_APP`` and
``FLASK_DEBUG`` environment variables ::

    export FLASK_APP=autoapp.py
    export FLASK_DEBUG=1

Once you have installed your DBMS, run the following to create your app's
database tables and perform the initial migration ::

    flask db init
    flask db migrate
    flask db upgrade
    npm start


Deployment
----------

To deploy::

    export FLASK_DEBUG=0
    npm run build   # build assets with webpack
    flask run       # start the flask server

In your production environment, make sure the ``FLASK_DEBUG`` environment
variable is unset or is set to ``0``, so that ``ProdConfig`` is used.


Shell
-----

To open the interactive shell, run ::

    flask shell

By default, you will have access to the flask ``app``.


Running Tests
-------------

To run all tests, run ::

    flask test


Migrations
----------

Whenever a database migration needs to be made. Run the following commands ::

    flask db migrate

This will generate a new migration script. Then run ::

    flask db upgrade

To apply the migration.

For a full migration command reference, run ``flask db --help``.


Asset Management
----------------

Files placed inside the ``assets`` directory and its subdirectories
(excluding ``js`` and ``css``) will be copied by webpack's
``file-loader`` into the ``static/build`` directory, with hashes of
their contents appended to their names.  For instance, if you have the
file ``assets/img/favicon.ico``, this will get copied into something
like
``static/build/img/favicon.fec40b1d14528bf9179da3b6b78079ad.ico``.
You can then put this line into your header::

    <link rel="shortcut icon" href="{{asset_url_for('img/favicon.ico') }}">

to refer to it inside your HTML page.  If all of your static files are
managed this way, then their filenames will change whenever their
contents do, and you can ask Flask to tell web browsers that they
should cache all your assets forever by including the following line
in your ``settings.py``::

    SEND_FILE_MAX_AGE_DEFAULT = 31556926  # one year
