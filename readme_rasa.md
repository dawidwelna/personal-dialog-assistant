## linkage

    * RUN python -m spacy download en
    
    * python -m spacy download en_core_web_md
    * python -m spacy link en_core_web_md en

python -m spacy download en

## run in terminal
    * cd rasa_movie_bot
    * python -m rasa_core.run --enable_api -c rest  -d data/movie_bot_en/models/dialogue \
             -u data/movie_bot_en/models/movie_bot/model_en --credentials credentials.yml --debug

    * open other terminal and send messages
        * curl -X POST localhost:5005/webhooks/rest/webhook -d '{"user": "user1", "message": "I want to watch movie"}' \ 
        -H 'Content-Type: application/json'


## Training


    
    * Train dialogue model
         - python -m rasa_core.train -d rasa_movie_bot/data/movie_bot_en/domain.yml \
                  -s rasa_movie_bot/data/movie_bot_en/story/stories.md \
                  -o rasa_movie_bot/data/movie_bot_en/models/dialogue/ --epochs 500

    * Train nlu    
         - python -m rasa_nlu.train --config rasa_movie_bot/data/configs/config.yml --data rasa_movie_bot/data/movie_bot_en/nlu/nlu.md \
                  -o rasa_movie_bot/data/movie_bot_en/models/ --fixed_model_name model_en --project movie_bot --verbose


## visualize the stories
    
    * python -m rasa_core.visualize -d rasa_movie_bot/data/movie_bot_en/domain.yml \
             -s rasa_movie_bot/data/movie_bot_en/story/stories.md -o graph.png

