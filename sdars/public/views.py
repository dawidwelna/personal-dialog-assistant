# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import Flask, Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user
import requests
from sdars.extensions import login_manager
from sdars.public.forms import LoginForm
from sdars.user.forms import RegisterForm
from sdars.user.models import User
from sdars.utils import flash_errors
from sdars.settings import Config
import json
import logging
from builtins import str


blueprint = Blueprint('public', __name__, static_folder='../static')

basedir = Config.PROJECT_ROOT

logger = logging.getLogger(__name__)

app = Flask(__name__)


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    return User.get_by_id(int(user_id))


@blueprint.route('/', methods=['GET', 'POST'])
def home():
    """Home page."""
    return render_template("public/home.html")


# @app.route('/get_movie_detail', methods=['POST'])
# def get_movie_detail():
#     data = request.get_json(silent=True)
#
#     try:
#         movie = data['queryResult']['parameters']['movie']
#         api_key = os.getenv('OMDB_API_KEY')
#
#         movie_detail = requests.get('http://www.omdbapi.com/?t={0}&apikey={1}'.format(movie, api_key)).content
#         movie_detail = json.loads(movie_detail)
#
#         response = """
#             Title : {0}
#             Released: {1}
#             Actors: {2}
#             Plot: {3}
#         """.format(movie_detail['Title'], movie_detail['Released'], movie_detail['Actors'], movie_detail['Plot'])
#     except:
#         response = "Could not get movie detail at the moment, please try again"
#
#     reply = {
#         "fulfillmentText": response,
#     }
#
#     return jsonify(reply)


url = "http://rasa_en:5005/webhooks/rest/webhook"


@blueprint.route("/get", methods=["GET", "POST"])
def get_bot_response():
    user_message = request.args.get('msg')
    # responses = agent.handle_message(user_message)
    #
    # print(responses)
    #
    # return str(responses)
    headers = {'content-type': 'application/json'}
    payload = {'sender': 'user1', 'message': user_message}
    r = requests.post(url, data=json.dumps(payload), headers=headers).text
    return r


# @app.route('/get', methods=["POST"])
# def get_bot_response():
#     """
#     chat end point that performs NLU using rasa.ai
#     and constructs response from response.py
#     """
#     # try:
#     response = requests.get("http://localhost:5000/parse", params={"q": request.form["text"]})
#     response = response.json()
#     print(response)
#     intent = response.get("intent", {}).get("name", "default")
#     entities = format_entities(response.get("entities", []))
#     if intent == "event-request":
#         response_text = get_event(entities["day"], entities["time"], entities["place"])
#     else:
#         response_text = get_random_response(intent)
#     return jsonify({"status": "success", "response": response_text})


@blueprint.route('/logout/')
@login_required
def logout():
    """Logout."""
    logout_user()
    flash('You are logged out.', 'info')
    return redirect(url_for('public.home'))


@blueprint.route('/register/', methods=['GET', 'POST'])
def register():
    """Register new user."""
    form = RegisterForm(request.form)
    if form.validate_on_submit():
        User.create(username=form.username.data, email=form.email.data, password=form.password.data, active=True)
        flash('Thank you for registering. You can now log in.', 'success')
        return redirect(url_for('public.home'))
    else:
        flash_errors(form)
    return render_template('public/register.html', form=form)


@blueprint.route('/about/')
def about():
    """About page."""
    form = LoginForm(request.form)
    return render_template('public/about.html', form=form)
